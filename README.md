# CubeSumation
Python solution of the CodeSumation problem described at: https://www.hackerrank.com/challenges/cube-summation/problem

##Implementation
The solution proposed is a simple map implementation where the key is a tuple (x, y, z) with the coordinates of the element.

Initially the map is empty, and its updated adding (key, value) pairs. If a queried element is not present the value 0 is returned by default.

The sum of a range operation is performed by iterating through the map and verifying if the key is between the given range.

This implementation was chosen because the number of operations is at most 1.000 so in the worst case, there will be 500 UPDATES and 500 QUERIES, each query would take O(M/2) time so the whole queries would take at most O(M^2/4) time. 

##Architecture and Deployment


