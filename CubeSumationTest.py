import unittest
from CubeSumation import execute_update, execute_query, MyMatrix
import numpy
from random import randint

class CubeSumationTest(unittest.TestCase):

    def test_assertion_working(self):
        self.assertRaises(AssertionError, self.assertTrue, False)

    def test_update_scalar_matrix(self):
        matrix = MyMatrix()
        execute_update("UPDATE 1 1 1 1220000000", matrix)
        self.assertEqual(matrix[1, 1, 1], 1220000000)

    def test_update_matrix(self):
        matrix = MyMatrix()
        xyz_list = list()
        w_list = list()
        for i in range(0, 10000):
            (x, y, z) = (randint(1, 50), randint(1, 50), randint(1, 50))
            w = randint(int(-1E9), int(1E9))
            #The following two lines are used to avoid testing errors.
            #For example, if there are two updates to the same coordinate
            #Or if the value of the update is zero
            if matrix[x, y, z] != 0: continue
            if w == 0: continue
            #------------------------------------------------------------
            xyz_list.append((x, y, z))
            w_list.append(w)
            execute_update("UPDATE "+str(x)+" "+str(y)+" "+str(z)+" "+str(w), matrix)
        for i in range(0, len(w_list)):
            (x, y, z) = xyz_list[i]
            w = w_list[i]
            self.assertEqual(matrix[x, y, z], w)

    def test_update_big_matrix(self):
        matrix = MyMatrix()
        xyz_list = list()
        w_list = list()
        for i in range(0, 10000):
            (x, y, z) = (randint(1, 100), randint(1, 100), randint(1, 100))
            w = randint(int(-1E9), int(1E9))
            #The following two lines are used to avoid testing errors.
            #For example, if there are two updates to the same coordinate
            #Or if the value of the update is zero
            if matrix[x, y, z] != 0: continue
            if w == 0: continue
            #------------------------------------------------------------
            xyz_list.append((x, y, z))
            w_list.append(w)
            execute_update("UPDATE "+str(x)+" "+str(y)+" "+str(z)+" "+str(w), matrix)
        for i in range(0, len(w_list)):
            (x, y, z) = xyz_list[i]
            w = w_list[i]
            self.assertEqual(matrix[x, y, z], w)

    def test_zero_and_multiple_updates(self):
        matrix = MyMatrix()
        execute_update("UPDATE 39 100 100 13577123", matrix)
        self.assertEqual(matrix[39, 100, 100], 13577123)
        execute_update("UPDATE 39 100 100 -12377460", matrix)
        self.assertEqual(matrix[39, 100, 100], -12377460)
        execute_update("UPDATE 39 100 100 99584589", matrix)
        self.assertEqual(matrix[39, 100, 100], 99584589)
        execute_update("UPDATE 12 89 100 -512531", matrix)
        self.assertEqual(matrix[12, 89, 100], -512531)
        execute_update("UPDATE 12 89 100 0", matrix)
        self.assertEqual(matrix[12, 89, 100], 0)

    def test_execute_query_scalar_matrix(self):
        matrix = MyMatrix()
        execute_update("UPDATE 1 1 1 32512611", matrix)
        self.assertEqual(execute_query("QUERY 1 1 1 1 1 1", matrix), 32512611)

    def test_execute_query_matrix(self):
        for j in range(0, 200):
            matrix = MyMatrix()
            (x1, y1, z1) = (randint(1, 22), randint(1, 22), randint(1, 22))
            (x2, y2, z2) = (randint(x1, 22), randint(y1, 22), randint(z1, 22))
            total_sum = 0
            for j in range(0, 100):
                (x, y, z) = (randint(1, 22), randint(1, 22), randint(1, 22))
                w = randint(int(-1E9), int(1E9))
                #The following two lines are used to avoid testing errors.
                #For example, if there are two updates to the same coordinate
                #Or if the value of the update is zero
                if matrix[x, y, z] != 0: continue
                if w == 0: continue
                #------------------------------------------------------------
                execute_update("UPDATE "+str(x)+" "+str(y)+" "+str(z)+" "+str(w), matrix)
                if x <= x2 and y <= y2 and z <= z2 and x >= x1 and y >= y1 and z >= z1:
                    total_sum += w
            query = "QUERY "+str(x1)+" "+str(y1)+" "+str(z1)+" "+ str(x2) + " "+str(y2)+" "+str(z2)
            self.assertEqual(execute_query(query, matrix), total_sum)

    def test_execute_big_query_matrix(self):
        for j in range(0, 1000):
            matrix = MyMatrix()
            (x1, y1, z1) = (randint(1, 99), randint(1, 99), randint(1, 99))
            (x2, y2, z2) = (randint(x1, 99), randint(y1, 99), randint(z1, 99))
            total_sum = 0
            for j in range(0, 1000):
                (x, y, z) = (randint(1, 99), randint(1, 99), randint(1, 99))
                w = randint(int(-1E9), int(1E9))
                #The following two lines are used to avoid testing errors.
                #For example, if there are two updates to the same coordinate
                #Or if the value of the update is zero
                if matrix[x, y, z] != 0: continue
                if w == 0: continue
                #------------------------------------------------------------
                execute_update("UPDATE "+str(x)+" "+str(y)+" "+str(z)+" "+str(w), matrix)
                if x <= x2 and y <= y2 and z <= z2 and x >= x1 and y >= y1 and z >= z1:
                    total_sum += w
            query = "QUERY "+str(x1)+" "+str(y1)+" "+str(z1)+" "+ str(x2) + " "+str(y2)+" "+str(z2)
            self.assertEqual(execute_query(query, matrix), total_sum)

if __name__ == '__main__':
    unittest.main()
