"""
Program developed to solve the cube sumation problem specified at:
    https://www.hackerrank.com/challenges/cube-summation/problem
Programmer: Daniel Fonnegra
E-mail: dfonnegrag@gmail.com
Date: May 05 2018
"""


class MyMatrix():
    """
    This class represents the mathematical concept of a 3D Matrix.
    """

    def __init__(self):
        """
        Abstraction function:
            3 dimensional dinamic matrix represented by a dictionary where
            - key = tuple(x, y, z) where x, y, z are the coordinates of the array
            The elements without key in the dictionary are supposed 0 by default.
        """
        self.matrix = dict()

    def __getitem__(self, item):
        return self.matrix.get(item, 0)

    def __setitem__(self, key, value):
        self.matrix[key]=value

    def sum_range(self, x1, y1, z1, x2, y2, z2):
        '''
        :param x1, y1, z1: Starting point of the sum range
        :param x2, y2, z2: Ending point of the sum range
        :return: Sum of the "sub-cube" with (x1, y1, z1) and (x2, y2, z2) as coordinates of the
            vertexes of one diagonal.
        '''
        sum_val = 0
        for key, value in self.matrix.items():
            (x, y, z) = key
            if x <= x2 and y <= y2 and z <= z2 and x >= x1 and y >= y1 and z >= z1:
                sum_val += value
        return sum_val

def execute_update(command, matrix):
    ''' Executes the update command on the given matrix. This method mutates the given matrix
    :param command: UPDATE command with format specified at:
        https://www.hackerrank.com/challenges/cube-summation/proble
    :param matrix: Input matrix which will be updated.
    '''
    split_cmd = command.split(' ')
    (x, y, z, w) = (int(split_cmd[1]), int(split_cmd[2]), int(split_cmd[3]), int(split_cmd[4]))
    matrix[x, y, z] = w


def execute_query(command, matrix):
    ''' Executes the query and returns the value.
    :param command: QUERY command with format specified at:
        https://www.hackerrank.com/challenges/cube-summation/proble
    :param matrix: Input matrix which will be updated.
    :return: Value of the query, the description of the output is specified at:
        https://www.hackerrank.com/challenges/cube-summation/proble
    '''
    split_cmd = command.split(' ')
    (x1, y1, z1) = (int(split_cmd[1]), int(split_cmd[2]), int(split_cmd[3]))
    (x2, y2, z2) = (int(split_cmd[4]), int(split_cmd[5]), int(split_cmd[6]))
    return matrix.sum_range(x1, y1, z1, x2, y2, z2)


def execute_command_list(command_list, matrix_size):
    ''' Executes the command list and outputs the answer through the terminal.
    :param command_list: List of commands with format specified at:
        https://www.hackerrank.com/challenges/cube-summation/problem
    '''
    matrix = MyMatrix()
    for command in command_list:
        if command.startswith("UPDATE"):
            execute_update(command, matrix)
        elif command.startswith("QUERY"):
            print(execute_query(command, matrix))


def main():
    n_tests = int(input())

    while n_tests > 0:
        sizes = input().split()
        matrix_size = int(sizes[0])
        n_operations = int(sizes[1])

        command_list = list()
        while n_operations > 0:
            command_list.append(input())
            n_operations -= 1

        execute_command_list(command_list, matrix_size)
        n_tests -= 1

if __name__ == '__main__':
    main()
